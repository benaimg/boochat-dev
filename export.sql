-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table tests.boochat_messages
CREATE TABLE IF NOT EXISTS `boochat_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) unsigned DEFAULT NULL,
  `body` text COLLATE utf8mb4_bin NOT NULL,
  `spam_score` float NOT NULL,
  `validated` bit(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `validated_at` (`updated_at`),
  KEY `validated` (`validated`),
  KEY `FK_boochat_messages_boochat_users` (`author_id`),
  KEY `FK2` (`updated_by`),
  CONSTRAINT `FK2` FOREIGN KEY (`updated_by`) REFERENCES `boochat_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_boochat_messages_boochat_users` FOREIGN KEY (`author_id`) REFERENCES `boochat_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5516 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table tests.boochat_teams
CREATE TABLE IF NOT EXISTS `boochat_teams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `code` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `color` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `background` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `text_color` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

-- Dumping structure for table tests.boochat_users
CREATE TABLE IF NOT EXISTS `boochat_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned DEFAULT NULL,
  `inp_id` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(3) unsigned NOT NULL COMMENT '0 (banned) - 255 (admin)',
  `can_post_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inp_id` (`inp_id`),
  UNIQUE KEY `nickname` (`nickname`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `FK_boochat_users_boochat_teams` FOREIGN KEY (`team_id`) REFERENCES `boochat_teams` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

INSERT INTO `boochat_teams` (`id`, `name`, `code`, `color`, `background`, `text_color`, `order_id`) VALUES
	(1, 'net7', 'net7', 16777215, 0, 16777215, 12),
	(2, 'TVn7', 'tvn7', 16777215, 0, 16777215, 11),
	(3, 'CAn7', 'can7', 16777215, 0, 16777215, 10),
	(4, 'Gat7by les Magnifiques', 'gat7by', 15709542, 12085, 15709542, 2),
	(5, 'N7rix et Obéliste', 'n7rix', 16769986, 16769986, 5242880, 1),
	(6, 'Ex7sior', 'ex7sior', 16711680, 16711680, 0, 3),
	(7, '7tés d\'Or', '7tesdor', 16300837, 16300837, 5242880, 4),
	(8, 'O\'N7 117', 'on7117', 16744272, 16744272, 0, 6),
	(9, 'L\'Ody7', 'ody7', 2263176, 1144695, 16777215, 5),
	(10, 'SN', 'sn', 15675474, 16729139, 0, 7),
	(11, 'EEEA', 'eeea', 15073024, 15073024, 0, 8),
	(12, 'MFEE', 'mfee', 6193382, 6193382, 0, 9);
	
INSERT INTO `boochat_users` (`id`, `team_id`, `inp_id`, `nickname`, `level`, `can_post_at`) VALUES
	(1, 1, 'benaimg', 'BooBot', 255, '2020-02-28 15:04:47');
