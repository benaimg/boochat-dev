-- phpMyAdmin SQL Dump
-- version 5.0.0
-- https://www.phpmyadmin.net/
--
-- Host: discover.bde.inp
-- Generation Time: Sep 30, 2020 at 02:21 PM
-- Server version: 10.3.23-MariaDB-0+deb10u1
-- PHP Version: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boochat`
--

-- --------------------------------------------------------

--
-- Table structure for table `boochat_teams`
--

CREATE TABLE `boochat_teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `code` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `color` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `background` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `text_color` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `boochat_teams`
--

INSERT INTO `boochat_teams` (`id`, `name`, `code`, `color`, `background`, `text_color`, `order_id`) VALUES
(1, 'net7', 'net7', 16777215, 0, 16777215, 12),
(2, 'TVn7', 'tvn7', 16777215, 0, 16777215, 11),
(3, 'CAn7', 'can7', 16777215, 0, 16777215, 10),
(4, 'Gat7by les Magnifiques', 'gat7by', 15709542, 12085, 15709542, 2),
(5, 'N7rix et Obéliste', 'n7rix', 16769986, 16769986, 5242880, 1),
(6, 'Ex7sior', 'ex7sior', 16711680, 16711680, 0, 3),
(7, '7tés d\'Or', '7tesdor', 16300837, 16300837, 5242880, 4),
(8, 'O\'N7 117', 'on7117', 16744272, 16744272, 0, 6),
(9, 'L\'Ody7', 'ody7', 2263176, 1144695, 16777215, 5),
(10, 'SN', 'sn', 15675474, 16729139, 0, 7),
(11, 'EEEA', 'eeea', 15073024, 15073024, 0, 8),
(12, 'MFEE', 'mfee', 6193382, 6193382, 0, 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boochat_teams`
--
ALTER TABLE `boochat_teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boochat_teams`
--
ALTER TABLE `boochat_teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

