/* eslint-disable no-unused-vars */
import path from 'path'
import express from 'express'
import helmet from 'helmet'
import consolidate from 'consolidate'

import type Conf from 'conf'
import type mysql from 'mysql'
import type CasAuthentication from 'cas-authentication'
import type MysqlApp from './mysql-app'
import type { UserLoggedIn } from './interfaces'

export default function expressApp ({
  mysqlApp,
  appConfig,
  expressSession,
  cas
}: {
  mysqlApp: MysqlApp
  appConfig: Conf,
  expressSession: express.RequestHandler,
  cas: CasAuthentication
}): express.Express {
  const app: express.Express = express()
  const publicRoot: string = path.join(__dirname, '..', 'build', 'public')

  // Configure helmet and hogan.js
  app.use(helmet())
  app.engine('html', consolidate.hogan)
  app.set('views', path.join(__dirname, '..', 'build', 'public'))

  // It's always nice to have a favicon
  app.get('/favicon.ico', (req, res) => {
    res.sendFile(path.join(publicRoot, 'favicon.ico'))
  })

  // Serve static files
  app.all(/\.(png|jpg|svg|js|css|map)$/, express.static(path.join(publicRoot)))

  // We share sessions between express and socket.io
  app.use(expressSession, (req, res, next) => {
    // Ensure req.session is defined in all subsequent middlewares
    if (!req.session) {
      res.sendStatus(500)
      throw new Error('Cannot open session')
    }
    next()
  })

  // Ensure authentification through the CAS
  app.get('/login', cas.bounce, (req, res) => {
    res.redirect('.')
  })
  app.get('/logout', cas.logout)
  app.use((req, res, next) => {
    if (!(req.session as Express.Session)[cas.session_name]) {
      res.sendFile(path.join(publicRoot, 'splash.html'))
      return
    }
    next()
  })

  // Personal account page /me
  app.route('/me.html').get((req, res) => {
    // Is the user registered?
    // @ts-ignore
    if (req?.user) {
      res.redirect('.')
      return
    }
    let errors: string[] = []
    if (req?.session?.formErrors) {
      errors = req.session.formErrors
      delete req.session.formErrors
    }
    mysqlApp.getTeams().then((teams) => {
      res.render('me.html', {
        teams: teams.map((team) => {
          return {
            ...team,
            color: '#' + team.color.toString(16).padStart(6, '0'),
            background: '#' + team.background.toString(16).padStart(6, '0'),
            textColor: '#' + team.textColor.toString(16).padStart(6, '0')
          }
        }),
        errors: {
          nicknameInUse: errors.indexOf('E_NICKNAME_IN_USE') > -1,
          emptyFields: errors.indexOf('E_EMPTY_FIELDS') > -1,
          teamUnknown: errors.indexOf('E_TEAM_UNKNOWN') > -1,
          unknownError: errors.indexOf('E_UNKNOWN_ERROR') > -1,
          badNickname: errors.indexOf('E_BAD_NICKNAME') > -1
        }
      })
    }).catch((error) => {
      console.error(error)
    })
  }).post(express.urlencoded({ extended: true }), (req, res) => {
    // @ts-ignore
    const session: Express.Session = req.session
    session.formErrors = session.formErrors || []

    // The request contains both a team and a nickname
    if (!req.body.team || !req.body.nickname) {
      session.formErrors.push('E_EMPTY_FIELDS')
      res.redirect('me.html')
      return
    }

    // Check that nickname is aplphanumeric
    if (!req.body.nickname.match(/^[a-zA-Z0-9_.-]{3,20}$/)) {
      session.formErrors.push('E_BAD_NICKNAME')
      res.redirect('me.html')
      return
    }

    // Register the user
    mysqlApp.insertUser({
      inpId: (req.session as Express.Session)[cas.session_name],
      teamId: req.body.team,
      nickname: req.body.nickname,
      level: appConfig.get('levels.chat')
    }).then((insertId) => {
      // User created, log them in
      res.redirect('.')
    }).catch((error) => {
      // Something went wrong, let's try to figure it out
      if ('code' in error) {
        if (error.code === 'ER_NO_REFERENCED_ROW_2' ||
            error.code === 'ER_WARN_DATA_OUT_OF_RANGE' ||
            error.code === 'ER_TRUNCATED_WRONG_VALUE_FOR_FIELD') {
          session.formErrors.push('E_TEAM_UNKNOWN')
          res.redirect('me.html')
          return
        } else if (error.code === 'ER_DUP_ENTRY') {
          session.formErrors.push('E_NICKNAME_IN_USE')
          res.redirect('me.html')
          return
        }
      }
      console.error(error)
      session.formErrors.push('E_UNKNOWN_ERROR')
      res.redirect('me.html')
    })
  })

  // Populate req.session.user if the user has an account,
  // or else send them to /me to create one
  app.use((req, res, next) => {
    // @ts-ignore
    const session: Express.Session = req.session

    // The user is logged in through the CAS, but we've not yet fetched
    // their details
    // @ts-ignore
    if (!req.user) {
      mysqlApp.getUserByInpId(session[cas.session_name]).then(user => {
        // @ts-ignore
        req.user = user
        next()
      }).catch((error) => {
        // The user has not yet created an account
        if (error instanceof ReferenceError) {
          res.redirect('./me.html')
          return
        }
        throw error
      }).catch((error) => {
        res.send('Wsh déso ça a plantey')
        console.error(error)
      })
      return
    }

    next()
  })

  // A bit of permission checking here and there
  app.all('/mod.html', (req, res, next) => {
    // @ts-ignore
    if (req.user.level < appConfig.get('levels.moderator')) {
      res.sendStatus(401)
      return
    }
    next()
  })

  // Serve static .html files
  app.use(express.static(path.join(publicRoot)))

  return app
}
