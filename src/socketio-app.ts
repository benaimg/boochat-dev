/* eslint-disable no-unused-vars */
import computeSpamScore from './spam-score'

import type Conf from 'conf'
import type socketio from 'socket.io'
import type express from 'express'
import type CasAuthentication from 'cas-authentication'
import type { DetailedMessage, UserLoggedIn, Message, Team } from './interfaces'
import type MysqlApp from './mysql-app'
import yargs from 'yargs'

export default function socketioApp ({
  mysqlApp,
  appConfig,
  io,
  expressSession,
  cas
}: {
  mysqlApp: MysqlApp,
  appConfig: Conf,
  io: socketio.Server,
  expressSession: express.RequestHandler,
  cas: CasAuthentication
}) {
  // We trick expressSession to populate socket.handshake.session,
  // since socket.handshake looks like a request
  // https://github.com/xpepermint/socket.io-express-session/blob/master/index.js
  io.use((socket, next) => {
    // @ts-ignore
    expressSession(socket.handshake, {}, next)
  })

  // Log user in
  io.use((socket, next) => {
    // @ts-ignore
    if (!socket.handshake?.session) {
      socket.error({ code: 'E_NO_SESSION', message: 'Erreur du serveur' })
      socket.disconnect(true)
      return
    }
    // @ts-ignore
    const session: Express.Session = socket.handshake.session

    if (!(cas.session_name in session)) {
      socket.error({ code: 'E_LOGGED_OUT', message: 'Vous êtes déconnecté, rafraîchissez la page' })
      socket.disconnect(true)
      return
    }

    mysqlApp.getUserByInpId(session[cas.session_name]).then((user) => {
      // Populate socket.user with user info
      // @ts-ignore
      socket.user = user
      // @ts-ignore
      socket.handshake.session.user = socket.user
      next()
    }).catch((error) => {
      socket.error({ code: 'E_UNKNOWN_USER', message: 'Vous êtes déconnecté, rafraîchissez la page' })
      socket.disconnect(true)
      console.error(error)
    })
  })

  // Maps user names with socket ids
  const socketMap: Map<string, Set<socketio.Socket & {user: UserLoggedIn}>> = new Map()

  // Connection listener
  return (socket: socketio.Socket & {user: UserLoggedIn}) => {
    const user: UserLoggedIn = socket.user
    const reloadUser = (user: UserLoggedIn) => {
      mysqlApp.getUserByInpId(user.inpId).then(updatedUser => {
        Object.assign(user, updatedUser)
        // Check updated persmissions
        if (!socket.handshake.query.mod) return
        if (user.level < appConfig.get('levels.moderator')) {
          socket.error({ code: 'E_LEVEL', message: 'Accès refusé' })
          socket.disconnect(true)
        }
      }).catch((error) => {
        console.error(error)
      })
    }

    // Save the socket id
    if (!socketMap.get(user.nickname)?.has(socket)) {
      if (!socketMap.has(user.nickname)) {
        socketMap.set(user.nickname, new Set())
      }
      socketMap.get(user.nickname)!.add(socket)
    }

    socket.on('disconnect', () => {
      socketMap.get(user.nickname)!.delete(socket)
    })

    if (user.level >= appConfig.get('levels.moderator')) {
      // Send logs to mods
      socket.join('see-logs')
    } else {
      // Limit to one connection per user
      socketMap.get(user.nickname)!.forEach((otherSocket) => {
        if (socket.id !== otherSocket.id) {
          otherSocket.error({
            code: 'E_TOO_MANY',
            message: 'Chat ouvert dans un autre onglet'
          })
          otherSocket.disconnect(true)
        }
      })
    }

    // Send teams to the client
    mysqlApp.getTeams().then((teams: Team[]) => {
      socket.emit('team-update', teams)
      return mysqlApp.getLatestMessages(appConfig.get('chat.history_length'))
    }).then((messages: Message[]) => {
      socket.emit('history', messages)
    }).catch((error) => {
      console.error(error)
    })

    // Receive incoming messages
    socket.on('message', (body: string) => {
      body = String(body)

      if (user.level < appConfig.get('levels.chat')) {
        socket.error({ code: 'E_LEVEL', message: 'Accès refusé' })
        socket.disconnect(true)
        return
      }

      const isCommand = body.length >= 1 && body[0] === '/'
      const spamScore = computeSpamScore(body)
      const isModerator = user.level >= appConfig.get('levels.moderator')
      const validated = !isCommand && (isModerator || spamScore < appConfig.get('spam_scores.moderated'))
      const delay = isModerator ? 0 : appConfig.get('chat.moderation_delay')
      const canPost = (new Date(user.canPostAt).valueOf() - new Date().valueOf()) <= 0

      if (!canPost) {
        socket.error({ code: 'E_SLOWDOWN', message: 'Pas si vite entre deux messages !' })
        return
      }

      // Save the message
      mysqlApp.insertMessage({
        authorId: user.id,
        body,
        spamScore,
        validated,
        moderationDelay: delay
      }).then((insertId) => {
        const message: Message = {
          id: insertId,
          body: body,
          author: {
            nickname: user.nickname,
            team: {
              id: user.teamId
            }
          }
        }
        const detailedMessage: DetailedMessage = {
          ...message,
          spamScore: spamScore,
          validated: validated,
          shown: false,
          author: {
            ...message.author,
            id: user.id,
            inp_id: user.inpId,
            level: user.level
          }
        }

        // Update slowdown
        if (!isModerator) {
          mysqlApp.updateUserCanPostAt({
            id: user.id,
            interval: appConfig.get('chat.slowdown')
          })
          user.canPostAt = new Date(
            new Date().valueOf() + appConfig.get('chat.slowdown') * 1000
          ).toISOString()
          socket.emit('chat-timings', {
            moderationDelay: appConfig.get('chat.moderation_delay'),
            slowdown: appConfig.get('chat.slowdown')
          })
        }

        // Is it a command?
        if (isCommand) {
          const half = (s: string, separator: string): [string, string] => {
            const pos = s.indexOf(separator)
            return pos === -1 ? [s, ''] : [s.substring(0, pos), s.substring(pos + separator.length)]
          }
          const [command, commandBody] = half(body.substring(1), ' ')
          io.to('see-logs').emit('notice', user.nickname + ': ' + body)

          try {
            // Commands below require moderator level
            if (user.level < appConfig.get('levels.moderator')) {
              throw new Error('Commande inconnue')
            }

            // Pong!
            if (command === 'ping') {
              socket.emit('notice', 'Pong!')
              return
            }

            // Whisper to mods
            if (command.startsWith('/')) {
              return
            }

            // Display help
            if (command === 'help') {
              socket.emit('notice', 'Commandes :\n//\n/ping\n/notice <message>\n/delay <value>\n/slow <value>\n/msg <nickname> <message>\n/set <key> <value>\n/help')
              return
            }

            // Send a message to everyone
            if (command === 'notice') {
              io.emit('notice', commandBody.trim())
              return
            }

            // Set the moderation delay
            if (command === 'delay') {
              const value = Math.max(Number(commandBody), 0)
              if (isNaN(value) || value < 0) {
                throw new Error('La valeur donnée doit être un nombre positif')
              }
              appConfig.set('chat.moderation_delay', value)
              io.to('see-logs').emit('notice', 'Délai de modération fixé à ' + value + 's')
              return
            }

            // Set the slowdown
            if (command === 'slow') {
              const value = Math.max(Number(commandBody), 0)
              if (isNaN(value) || value < 0) {
                throw new Error('La valeur donnée doit être un nombre positif')
              }
              appConfig.set('chat.slowdown', value)
              io.to('see-logs').emit('notice', 'Slowdown fixé à ' + value + 's')
              return
            }

            // List the moderators
            if (command === 'list') {
              if (socket.user.level < appConfig.get('levels.moderator')) {
                throw new Error('Accès refusé')
              }
              mysqlApp.getModerators(appConfig.get('levels.moderator')).then((users) => {
                var j:string='';
                for (let user of users) {
                  j += user.nickname + '(' + user.inpId + ') -> ' + user.level + '\n';
                }
                socket.emit('notice', 'Liste des modérateurs :\n' + j);
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }


            // Send a personnal message
            if (command === 'msg') {
              let [nickname, message] = half(commandBody, ' ')
              nickname = nickname.trim()
              message = message.trim()
              if (!nickname || !message) {
                throw new Error('Usage: /msg <nickname> <message>')
              }
              if (socketMap.has(nickname)) {
                socketMap.get(nickname)!.forEach((socket) => {
                  socket.emit('notice', message)
                })
                socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
              } else {
                socket.emit('notice', 'Utilisateur déconnecté')
              }
              return
            }

            // Force a user to reload their session
            if (command === 'reload') {
              const nickname = commandBody.trim()
              if (!nickname) {
                throw new Error('Usage: /reload <nickname>')
              }
              if (socketMap.has(nickname)) {
                socketMap.get(nickname)!.forEach((socket) => {
                  reloadUser(socket.user)
                })
                socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
              } else {
                socket.emit('notice', 'Utilisateur déconnecté')
              }
              return
            }

            // Rename a user
            if (command === 'nick') {
              let [nickname, newNickname] = half(commandBody, ' ')
              nickname = nickname.trim()
              newNickname = newNickname.trim()
              if (!nickname || !newNickname) {
                throw new Error('Usage: /nick <nickname> <newNickname>')
              }
              mysqlApp.getUserByNickname(nickname).then((user) => {
                if (socket.user.level < user.level) {
                  throw new Error('Accès refusé')
                }
                return mysqlApp.updateUser({
                  id: user.id,
                  nickname: newNickname,
                  teamId: user.teamId,
                  level: user.level
                })
              }).then(() => {
                if (socketMap.has(nickname)) {
                  socketMap.get(nickname)!.forEach((socket) => {
                    reloadUser(socket.user)
                  })
                  socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
                  socketMap.set(newNickname, socketMap.get(nickname)!)
                  socketMap.delete(nickname)
                } else {
                  socket.emit('notice', 'Utilisateur déconnecté')
                }
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }

            // Change a user's team
            if (command === 'team') {
              let [nickname, newTeam] = half(commandBody, ' ')
              nickname = nickname.trim()
              const newTeamId = Number(newTeam.trim())
              if (!nickname || isNaN(newTeamId) || newTeamId <= 0) {
                throw new Error('Usage: /team <nickname> <teamId>')
              }
              mysqlApp.getUserByNickname(nickname).then((user) => {
                if (socket.user.level < user.level) {
                  throw new Error('Accès refusé')
                }
                return mysqlApp.updateUser({
                  id: user.id,
                  nickname: user.nickname,
                  teamId: newTeamId,
                  level: user.level
                })
              }).then(() => {
                if (socketMap.has(nickname)) {
                  socketMap.get(nickname)!.forEach((socket) => {
                    reloadUser(socket.user)
                  })
                  socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
                } else {
                  socket.emit('notice', 'Utilisateur déconnecté')
                }
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }

            // Ban a user
            if (command === 'ban') {
              const nickname = commandBody.trim()
              if (!nickname) {
                throw new Error('Usage: /ban <nickname>')
              }
              mysqlApp.getUserByNickname(nickname).then((user) => {
                if (socket.user.level < user.level) {
                  throw new Error('Accès refusé')
                }
                return mysqlApp.updateUser({
                  id: user.id,
                  nickname: user.nickname,
                  teamId: user.teamId,
                  level: appConfig.get('levels.banned')
                })
              }).then(() => {
                if (socketMap.has(nickname)) {
                  socketMap.get(nickname)!.forEach((socket) => {
                    reloadUser(socket.user)
                    socket.error({ code: 'E_RELOAD', message: 'Rafraîchissez la page' })
                    socket.disconnect(true)
                  })
                  socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
                } else {
                  socket.emit('notice', 'Utilisateur déconnecté')
                }
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }

            //! TODO: refactor all user related commands
            // Reset a user's level
            if (command === 'chat') {
              const nickname = commandBody.trim()
              if (!nickname) {
                throw new Error('Usage: /chat <nickname>')
              }
              mysqlApp.getUserByNickname(nickname).then((user) => {
                if (socket.user.level < user.level) {
                  throw new Error('Accès refusé')
                }
                return mysqlApp.updateUser({
                  id: user.id,
                  nickname: user.nickname,
                  teamId: user.teamId,
                  level: appConfig.get('levels.chat')
                })
              }).then(() => {
                if (socketMap.has(nickname)) {
                  socketMap.get(nickname)!.forEach((socket) => {
                    reloadUser(socket.user)
                    socket.error({ code: 'E_RELOAD', message: 'Rafraîchissez la page' })
                    socket.disconnect(true)
                  })
                  socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
                } else {
                  socket.emit('notice', 'Utilisateur déconnecté')
                }
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }

            // Reset a user's level
            if (command === 'mod') {
              const nickname = commandBody.trim()
              if (!nickname) {
                throw new Error('Usage: /mod <nickname>')
              }
              mysqlApp.getUserByNickname(nickname).then((user) => {
                if (socket.user.level < user.level) {
                  throw new Error('Accès refusé')
                }
                return mysqlApp.updateUser({
                  id: user.id,
                  nickname: user.nickname,
                  teamId: user.teamId,
                  level: appConfig.get('levels.moderator')
                })
              }).then(() => {
                if (socketMap.has(nickname)) {
                  socketMap.get(nickname)!.forEach((socket) => {
                    reloadUser(socket.user)
                  })
                  socket.emit('notice', `Message envoyé à ${socketMap.get(nickname)!.size} socket(s)`)
                } else {
                  socket.emit('notice', 'Utilisateur déconnecté')
                }
              }).catch((error) => {
                socket.error({ code: 'E_COMMAND', message: error.message })
              })
              return
            }

            // Commands below require moderator level
            if (user.level < appConfig.get('levels.administrator')) {
              throw new Error('Commande inconnue')
            }

            // Set any other parameter
            if (command === 'set') {
              let [name, value]: [string, string | number] = half(commandBody, ' ')
              name = name.trim()
              value = value.trim()
              if (!name || !value) {
                throw new Error('Usage: /set <name> <value>')
              }
              if (!isNaN(Number(value))) {
                value = Number(value)
              }
              appConfig.set(name, value)
              socket.emit('notice', 'Set ' + name + '=' + value)
              return
            }

            throw new Error('Commande inconnue')
          } catch (error) {
            socket.error({ code: 'E_COMMAND', message: error.message })
          }
          return
        }

        // Send the message to moderators
        io.to('mod').emit('detailed-message', detailedMessage)

        // Send the message a few seconds later
        setTimeout(() => {
          mysqlApp.getMessageValidationState(insertId).then((validated) => {
            if (validated) {
              io.emit('message', message)
            }
          }).catch((error) => {
            console.error(error)
          })
        }, delay * 1000)
      }).catch((error) => {
        console.error(error)
      })
    })

    // Add the socket to the moderation room
    if (!socket.handshake.query.mod) return

    if (user.level < appConfig.get('levels.moderator')) {
      socket.error({ code: 'E_LEVEL', message: 'Accès refusé' })
      socket.disconnect(true)
      return
    }

    // Join the mod room and send a detailed history
    socket.join('mod')
    mysqlApp.getLatestDetailedMessages(
      appConfig.get('chat.history_length')
    ).then((detailedMessages: DetailedMessage[]) => {
      socket.emit('detailed-history', detailedMessages)
    })

    // Message deletion
    socket.on('delete-message', (id: number) => {
      id = Number(id)
      io.emit('delete-message', id)
      mysqlApp.updateMessageValidationState(id, user.id, false).catch((error) => {
        console.error(error)
      })
    })

    // Message approval
    socket.on('approve-message', (id: number) => {
      id = Number(id)
      mysqlApp.getMessage(id).then((message: Message) => {
        io.emit('message', message)
      }).catch((error) => {
        console.error(error)
      })
      mysqlApp.updateMessageValidationState(id, user.id, true).catch((error) => {
        console.error(error)
      })
    })
  }
}
