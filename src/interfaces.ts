export interface UserLoggedIn {
  id: number
  inpId: string
  teamId: number
  nickname: string
  level: number
  canPostAt: string
}

export interface Message {
  id: number
  author: {
    nickname: string
    team: {
      id: number
    }
  },
  body: string
}

export interface DetailedMessage extends Message {
  spamScore: number
  validated: boolean
  shown: boolean
  author: Message['author'] & {
    id: number
    // eslint-disable-next-line camelcase
    inp_id: string
    level: number
  }
}

export interface Team {
  id: number
  name: string
  code: string
  color: number
  background: number
  textColor: number
}

export interface Notice {
  id: number
  body: string
}
