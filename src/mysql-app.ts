/* eslint-disable no-unused-vars */
import type { UserLoggedIn, DetailedMessage, Message, Team } from './interfaces'
import type { Pool } from 'mariadb'

export default class MysqlApp {
  protected pool: Pool;

  constructor (pool: Pool) {
    this.pool = pool
  }

  async getUserByInpId (inpId: string): Promise<UserLoggedIn> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT u.id, u.nickname, u.level,
          u.team_id, u.can_post_at
        FROM boochat_users u
        WHERE u.inp_id = ?
        LIMIT 1`,
        inpId
      )
      if (rows.length === 0) {
        throw new ReferenceError(`User "${inpId}" unknown`)
      }
      return {
        id: rows[0].id,
        inpId,
        teamId: rows[0].team_id,
        nickname: rows[0].nickname,
        level: rows[0].level,
        canPostAt: rows[0].can_post_at
      } as UserLoggedIn
    } finally {
      connection.release()
    }
  }

  async getUserByNickname (nickname: string): Promise<UserLoggedIn> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT u.id, u.inp_id, u.nickname, u.level,
          u.team_id, u.can_post_at
        FROM boochat_users u
        WHERE u.nickname = ?
        LIMIT 1`,
        nickname
      )
      if (rows.length === 0) {
        throw new ReferenceError(`User "${nickname}" unknown`)
      }
      return {
        id: rows[0].id,
        inpId: rows[0].inp_id,
        teamId: rows[0].team_id,
        nickname: rows[0].nickname,
        level: rows[0].level,
        canPostAt: rows[0].can_post_at
      } as UserLoggedIn
    } finally {
      connection.release()
    }
  }

  async getModerators (level: number): Promise<Array<UserLoggedIn>> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT u.id, u.inp_id, u.nickname, u.level,
          u.team_id, u.can_post_at
        FROM boochat_users u
        WHERE u.level >= ?`,
        level
      )
      var res = []
      for (let v of rows) {
        res.push({
          id: v.id,
          inpId: v.inp_id,
          teamId: v.team_id,
          nickname: v.nickname,
          level: v.level,
          canPostAt: v.can_post_at
        } as UserLoggedIn)
      }
      return res
    } finally {
      connection.release()
    }
  }


  async getMessage (id: number): Promise<Message> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT m.body, u.nickname AS author_nickname, u.team_id AS author_team_id
        FROM boochat_messages m
        JOIN boochat_users u ON author_id = u.id
        WHERE m.id = ?
        LIMIT 1`,
        [id]
      )
      if (rows.length <= 0) throw new ReferenceError(`Message #${id} unknown`)
      return {
        id: id,
        body: rows[0].body,
        author: {
          nickname: rows[0].author_nickname,
          team: {
            id: rows[0].author_team_id
          }
        }
      } as Message
    } finally {
      connection.release()
    }
  }

  async getTeams (): Promise<Team[]> {
    const connection = await this.pool.getConnection()
    try {
      return await connection.query(
        `SELECT id, name, code, color, background, text_color AS textColor
        FROM boochat_teams
        ORDER BY order_id ASC`
      ) as Team[]
    } finally {
      connection.release()
    }
  }

  async insertUser ({
    inpId,
    teamId,
    nickname,
    level
  }: {
    inpId: string;
    teamId: number;
    nickname: string;
    level: number;
  }): Promise<number> {
    const connection = await this.pool.getConnection()
    try {
      const results = await connection.query(
        `INSERT INTO boochat_users(inp_id, team_id, nickname, level)
        VALUES(?, ?, ?, ?)`,
        [inpId, teamId, nickname, level]
      )
      return results.insertId
    } finally {
      connection.release()
    }
  }

  async updateUser ({
    id,
    teamId,
    nickname,
    level
  }: {
    id: number;
    teamId: number;
    nickname: string;
    level: number;
  }): Promise<void> {
    const connection = await this.pool.getConnection()
    try {
      const results = await connection.query(
        `UPDATE boochat_users
        SET team_id=?, nickname=?, level=?
        WHERE id=?
        LIMIT 1`,
        [teamId, nickname, level, id]
      )
      return
    } finally {
      connection.release()
    }
  }

  async insertMessage ({
    authorId,
    body,
    spamScore,
    validated,
    moderationDelay
  }: {
    authorId: number;
    body: string;
    spamScore: number;
    validated: boolean;
    moderationDelay: number;
  }): Promise<number> {
    const connection = await this.pool.getConnection()
    try {
      const response = await connection.query(
        `INSERT INTO boochat_messages(author_id, body, spam_score,
        validated, created_at, updated_at)
        VALUE(?, ?, ?, ?, NOW(), NOW() + INTERVAL ? SECOND)`,
        [authorId, body, spamScore, validated, moderationDelay]
      )
      return response.insertId
    } finally {
      connection.release()
    }
  }

  async getMessageValidationState (id: number): Promise<boolean> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT validated=1 AS validated
        FROM boochat_messages m
        WHERE m.id = ?
        LIMIT 1`,
        [id]
      )
      if (rows.length <= 0) throw new ReferenceError(`Message #${id} unknown`)
      return Boolean(rows[0].validated)
    } finally {
      connection.release()
    }
  }

  async updateMessageValidationState (
    id: number,
    updatedBy: number,
    validated: boolean
  ): Promise<void> {
    const connection = await this.pool.getConnection()
    try {
      await connection.query(
        `UPDATE boochat_messages
        SET validated=?, updated_at=NOW(), updated_by=?
        WHERE id=?
        LIMIT 1`,
        [validated, updatedBy, id]
      )
      return
    } finally {
      connection.release()
    }
  }

  async getLatestMessages (limit: number): Promise<Message[]> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT *
        FROM (
          SELECT m.id, m.body, u.nickname AS author_nickname, u.team_id AS author_team_id, m.updated_at
          FROM boochat_messages m
          JOIN boochat_users u ON author_id = u.id
          WHERE m.validated=1 AND m.updated_at <= NOW()
            AND SUBSTRING(m.body, 1, 1) != "/"
          ORDER BY m.updated_at DESC
          LIMIT ?
        ) m
        ORDER BY m.updated_at ASC`,
        [limit]
      )
      return rows.map((row: any) => {
        return {
          id: row.id,
          body: row.body,
          author: {
            nickname: row.author_nickname,
            team: {
              id: row.author_team_id
            }
          }
        } as Message
      })
    } finally {
      connection.release()
    }
  }

  async getLatestDetailedMessages (limit: number): Promise<DetailedMessage[]> {
    const connection = await this.pool.getConnection()
    try {
      const rows = await connection.query(
        `SELECT *
        FROM (
          SELECT
            m.id, m.body, m.updated_at, m.spam_score, m.validated = 1 AS validated,
            (m.validated AND m.updated_at < NOW()) AS shown,
            u.id AS author_id, u.nickname AS author_nickname, u.team_id AS author_team_id, u.inp_id AS author_inp_id, u.level AS author_level
          FROM boochat_messages m
          JOIN boochat_users u ON author_id = u.id
          WHERE SUBSTRING(m.body, 1, 1) != "/"
          ORDER BY m.id DESC
          LIMIT ?
        ) m
        ORDER BY m.id ASC`,
        [limit]
      )
      return rows.map((row: any) => {
        return {
          id: row.id,
          body: row.body,
          spamScore: row.spam_score,
          validated: Boolean(row.validated),
          shown: Boolean(row.shown),
          author: {
            id: row.author_id,
            inp_id: row.author_inp_id,
            nickname: row.author_nickname,
            level: row.author_level,
            team: {
              id: row.author_team_id
            }
          }
        } as DetailedMessage
      })
    } finally {
      connection.release()
    }
  }

  async updateUserCanPostAt (
    { id, interval }: { id: number; interval: number; }
  ): Promise<undefined> {
    const connection = await this.pool.getConnection()
    try {
      await connection.query(
        `UPDATE boochat_users
        SET can_post_at=NOW() + INTERVAL ? SECOND
        WHERE id=?
        LIMIT 1`,
        [interval, id]
      )
      return
    } finally {
      connection.release()
    }
  }
}
